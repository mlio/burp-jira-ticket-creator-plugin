package burp;

import ss.JiraPosterTab;
import ss.JiraPosterContextMenu;

import java.io.PrintStream;

/**
 * Created by mlio on 2/12/15.
 * Jira issue creator plugin
 */
public class BurpExtender implements IBurpExtender, IExtensionStateListener {

    private IBurpExtenderCallbacks callbacks;
    private JiraPosterTab jpt;
    PrintStream out;
    PrintStream err;

    @Override
    public void registerExtenderCallbacks(IBurpExtenderCallbacks callbacks) {

        this.callbacks = callbacks;
        jpt = new JiraPosterTab(callbacks);
        callbacks.setExtensionName("Jira Poster");
        callbacks.registerContextMenuFactory(new JiraPosterContextMenu(callbacks));
        callbacks.addSuiteTab(jpt);

        this.out = new PrintStream(callbacks.getStdout());
        this.err = new PrintStream(callbacks.getStderr());

        out.println("HELLO COMRADE!\n\n This plugin is used to send issues to JIRA. Check this console to see if the post failed or succeeded after sending an issue. ");
    }

    @Override
    public void extensionUnloaded() {

    }


}
