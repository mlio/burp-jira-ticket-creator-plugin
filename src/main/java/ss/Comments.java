package ss;

/**
 * Created by mlio on 2/12/15.
 * Sample comments for vulnerabilities
 */


public class Comments {

    public static final String rXSS() {

        String xss = "Cross-Site Scripting (XSS) attacks are a type of injection, in which malicious scripts are injected into otherwise benign and trusted web sites." +
                " XSS attacks occur when an attacker uses a web application to send malicious code, generally in the form of a browser side script, to a different end user. " +
                "Flaws that allow these attacks to succeed are quite widespread and occur anywhere a web application uses input from a user within the output it generates without validating or encoding it.\n" +
                "\n" +
                "An attacker can use XSS to send a malicious script to an unsuspecting user. The end user’s browser has no way to know that the script should not be trusted, and will execute the script." +
                " Because it thinks the script came from a trusted source, the malicious script can access any cookies, session tokens, or other sensitive information retained by the browser and used with that site." +
                " These scripts can even rewrite the content of the HTML page." +
                "\n\n[Remediation]\n\n" +
                "Java/HTML Escape Before Inserting Untrusted Data into HTML Element Content.\n";
        return xss;
    }


    public static String sXSS() {
        String xss = "Stored XSS vulnerability is a more devastating variant of a cross-site scripting flaw: it occurs when the data provided by the attacker is saved by the server, " +
                " and then permanently displayed on \"normal\" pages returned to other users in the course of regular browsing, without proper HTML escaping. \n" +
                "\n\n[Remediation]\n\n" +
                "Java/HTML Escape Before Inserting Untrusted Data into HTML Element Content.\n";

        return xss;
    }

    public static final String SQLi(){
        String sqli= "A SQL injection attack consists of insertion or \"injection\" of a SQL query via the input data from the client to the application." +
                " A successful SQL injection exploit can read sensitive data from the database, modify database data (Insert/Update/Delete)," +
                " execute administration operations on the database (such as shutdown the DBMS), recover the content of a given file present " +
                "on the DBMS file system and in some cases issue commands to the operating system. SQL injection attacks are a type of injection attack," +
                " in which SQL commands are injected into data-plane input in order to effect the execution of predefined SQL commands." +
                "\n\n[Remediation]\n\n" +
                "- Use prepared statements.\n" +
                "Escape\\sanitize all user input before putting it in a query or use prepared statements.\n\n";
        return sqli;
    }


    public static final String CSRF(){
        String csrf= "ross-Site Request Forgery (CSRF) is an attack which forces an end user to execute unwanted actions on a web application in which he/she " +
                "is currently authenticated. CSRF attacks specifically target state-changing requests, not theft of data, " +
                "since the attacker has no way to see the response to the forged request. With a little help of social engineering (like sending a link via email/chat), " +
                "an attacker may trick the users of a web application into executing actions of the attacker's choosing. If the victim is a normal user," +
                " a successful CSRF attack can force the user to perform state changing requests like transfering funds, " +
                "changing their email address, etc. If the victim is an administrative account, CSRF can compromise the entire web application." +
                "\n\n[Remediation]\n\n" +
                "Use a sufficiently long random token for each request to protect against CSRF attacks.\n\n";
        return csrf;
    }

    public static final String CMDi(){
        String cmdi= "Command injection is an attack in which the goal is execution of arbitrary commands on the host operating system via a vulnerable application. " +
                "Command injection attacks are possible when an application passes unsafe user supplied data (forms, cookies, HTTP headers etc.)" +
                " to a system shell. In this attack, the attacker-supplied operating system commands are usually executed with the privileges of the vulnerable application. " +
                "Command injection attacks are possible largely due to insufficient input validation." +
                "\n\n[Remediation]\n\n" +
                "Sanitize all input strings and do not allow un-validated code to access backend systems.\n" +
                "The web application and its components should also be running under strict permissions that do not allow operating system command execution.\n\n";
        return cmdi;
    }

}
