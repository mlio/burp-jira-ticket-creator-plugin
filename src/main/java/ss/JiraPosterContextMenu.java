package ss;

import burp.*;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mlio on 2/12/15.
 * context menu for poster tab
 */
public class JiraPosterContextMenu implements IContextMenuFactory{

    private IBurpExtenderCallbacks callbacks;
    private IExtensionHelpers helpers;
    private IHttpRequestResponse[] requestResponse;

    private JiraPosterTab jpt;
    private IScanIssue[] issues;
    private String scanIssue;
    private String scanDetail;
    private String scanBackground;
    private String scanRemediation;
    private String scanSeverity;
    private String jiraLevel;
    private URL scanURL;
    private IHttpRequestResponse message;

    private byte selectedContext;

    private PrintStream out = System.out;
    private PrintStream err = System.err;

    String reqres = "";
    URL reqresURL;


    public JiraPosterContextMenu(IBurpExtenderCallbacks callbacks){
        this.callbacks = callbacks;
        this.helpers = callbacks.getHelpers();
        this.out = new PrintStream(callbacks.getStdout());
        this.err = new PrintStream(callbacks.getStderr());
    }


    public List<JMenuItem> createMenuItems(final IContextMenuInvocation iContextMenuInvocation) {
        if (jpt == null) {
            jpt = new JiraPosterTab(callbacks);
        }

        List<JMenuItem> menu = new ArrayList<JMenuItem>();
        selectedContext = iContextMenuInvocation.getInvocationContext();
        requestResponse = iContextMenuInvocation.getSelectedMessages();

        //Checks if the context is a scanner result
        if (selectedContext == IContextMenuInvocation.CONTEXT_SCANNER_RESULTS){

            JMenuItem scannerIssue = new JMenuItem("Report Scanner Issue To Jira");
            issues = iContextMenuInvocation.getSelectedIssues();

            scannerIssue.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    //if multiple issues are selected itterate through them and post to jira...
                    for(int x = 0; x <issues.length; x++){
                        IScanIssue issue = issues[x];
                        scanIssue = issue.getIssueName();
                        scanBackground = issue.getIssueBackground();
                        scanDetail = issue.getIssueDetail();
                        scanRemediation = issue.getRemediationBackground();
                        scanSeverity = issue.getSeverity();
                        scanURL = issue.getUrl();
                        out.println("Scanner Issue: " + scanIssue + "\nScanner Severity: " + scanSeverity);

                        //Convert Scanner Issue into Jira Priority level
                        //Since there are only 4 levels Blocker is not used
                        //High = Critcal
                        //Medium = Major
                        //Low = Minor
                        //Information = Trivial
                        if(scanSeverity.equals("High")){
                            jiraLevel = "2";
                        }
                        else if(scanSeverity.equals("Medium")){
                            jiraLevel = "3";
                        }
                        else if(scanSeverity.equals("Low")){
                            jiraLevel = "4";
                        }
                        else if(scanSeverity.equals("Information")){
                            jiraLevel = "5";
                        }
                        if(scanDetail == null){
                            jpt.postIssue(scanIssue,scanBackground + "\n\n[ Affected URL ]\n\n" +scanURL.toString() + "\n\n[ Remediation ]\n\n" + scanRemediation ,jiraLevel);
                        }
                        else{
                            jpt.postIssue(scanIssue,scanBackground + "\n\n" + scanDetail + "\n\n[ URL ]\n\n" +scanURL.toString() + "\n\n[ Remediation ]\n\n" + scanRemediation ,jiraLevel);
                        }
                    }
                    issues=null;
                }
            });
            menu.add(scannerIssue);
        }
        //Load the default menu
        else
        {
            out.println("The selected context is ?!" + selectedContext);
            //Init menu items
            JMenu main = new JMenu("Create Jira Ticket");
            JMenu xss = new JMenu("XSS");
            JMenuItem rflt = new JMenuItem("Reflected");
            JMenuItem strd = new JMenuItem("Stored");
            JMenuItem sqli = new JMenuItem("SQLi");
            JMenuItem csrf = new JMenuItem("CSRF");
            JMenuItem cmdi = new JMenuItem("Command Injection");

            //From the selected context convert the message from bytes to string.
            for(int x = 0 ; x < iContextMenuInvocation.getSelectedMessages().length; x++){
                message = requestResponse[x];
                reqres += helpers.bytesToString(message.getRequest());
            }
            reqresURL = message.getUrl();


            //Add menus
            menu.add(main);
            main.add(xss);

            //Add menu Listeners then add to the menu
            rflt.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    jpt.postIssue("XSS", Comments.rXSS() + "[ Affected URL ]\n\n" + reqresURL +  "\n\n[ Sample Request ]\n\n" + reqres,"3");
                    reqres = "";
                }
            });

            strd.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    jpt.postIssue("XSS", Comments.sXSS() + "[ Affected URL ]\n\n" + reqresURL +  "\n\n[ Sample ]\n\n" +  reqres ,"3");
                    reqres = "";
                }
            });

            xss.add(rflt);
            xss.add(strd);

            sqli.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    jpt.postIssue("SQL Injection", Comments.SQLi() + "[ Affected URL ]\n\n" + reqresURL +  "\n\n[ Sample ]\n\n" +  reqres ,"2");
                    reqres = "";
                }
            });

            csrf.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    jpt.postIssue("Cross Site Request Forgery", Comments.CSRF() + "[ URL ]\n\n" + reqresURL +  "\n\n[ Sample ]\n\n" +  reqres,"3");
                    reqres = "";
                }
            });

            cmdi.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    jpt.postIssue("Command Injection",  Comments.CMDi() + "[ URL ]\n\n" + reqresURL +  "\n\n[ Sample ]\n\n" +  reqres,"1");
                    reqres= "";
                }
            });

            main.add(sqli);
            main.add(csrf);
            main.add(cmdi);
            //null out the request Response for the next invocation
            requestResponse = null;
        }

        return menu;
    }

}
