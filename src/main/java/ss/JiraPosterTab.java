package ss;

import burp.IBurpExtenderCallbacks;
import burp.IExtensionHelpers;
import burp.ITab;
import net.rcarz.jiraclient.*;

import javax.swing.*;
import java.awt.*;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintStream;
import java.util.concurrent.Executors;

/**
 * Created by mlio on 2/12/15.
 */

public class JiraPosterTab implements ITab {

    private String JIRA_USERNAME = "";
    private String JIRA_URL = "";
    private String JIRA_DEFAULT_ASSIGNEE = "";
    private String JIRA_TICKET = "";

    private BasicCredentials creds;
    private JiraClient jira;
    private Issue issue;

    private static JTextField jiraSite;
    private static JTextField jiraParentTicket;
    private static JTextField jiraAssignee;
    private static JTextField jiraUser;
    private static JPasswordField jiraPassword;
    private JButton testConnection;
    private JLabel info;
    private JLabel summary;
    private JLabel reporter;
    private JLabel assignee;
    private JLabel connTest;
    PrintStream out = System.out;
    PrintStream err = System.err;

    public JiraPosterTab(IBurpExtenderCallbacks callbacks){
        this.out = new PrintStream(callbacks.getStdout());
        this.err = new PrintStream(callbacks.getStderr());
    }

    @Override
    public String getTabCaption() {
        return "Jira Settings";
    }


    @Override
    public Component getUiComponent() {
        //Setup the tab and everything
        JPanel main = new JPanel();

        GridBagLayout gb = new GridBagLayout();
        GridBagConstraints cs = new GridBagConstraints();
        cs.fill = GridBagConstraints.HORIZONTAL;
        main.setLayout(gb);

        JLabel jiraSiteLabel = new JLabel("Enter Jira API Url ");
        cs.gridx = 0;
        cs.gridy = 0;
        cs.gridwidth =1;
        main.add(jiraSiteLabel,cs);

        jiraSite = new JTextField(20);
        cs.gridx = 1;
        cs.gridy = 0;
        cs.gridwidth = 2;
        main.add(jiraSite,cs);

        JLabel jiraParentTicketLabel = new JLabel("Enter Parent Ticket ");
        cs.gridx = 0;
        cs.gridy = 1;
        cs.gridwidth = 1;
        main.add(jiraParentTicketLabel,cs);

        jiraParentTicket = new JTextField(20);
        cs.gridx = 1;
        cs.gridy = 1;
        cs.gridwidth = 2;
        main.add(jiraParentTicket,cs);

        JLabel jiraAssigneeLabel = new JLabel("Enter Assignee ");
        cs.gridx = 0;
        cs.gridy = 2;
        cs.gridwidth = 1;
        main.add(jiraAssigneeLabel,cs);

        jiraAssignee = new JTextField(20);
        cs.gridx = 1;
        cs.gridy = 2;
        cs.gridwidth = 2;
        main.add(jiraAssignee,cs);

        JLabel jiraUserLabel = new JLabel("Jira Username ");
        cs.gridx = 0;
        cs.gridy = 3;
        cs.gridwidth =1;
        main.add(jiraUserLabel,cs);

        jiraUser = new JTextField(20);
        cs.gridx = 1;
        cs.gridy = 3;
        cs.gridwidth = 2;
        main.add(jiraUser,cs);

        JLabel jiraPass = new JLabel("Enter Jira Password ");
        cs.gridx = 0;
        cs.gridy = 4;
        cs.gridwidth = 1;
        main.add(jiraPass,cs);

        jiraPassword = new JPasswordField(20);
        cs.gridx = 1;
        cs.gridy = 4;
        cs.gridwidth = 2;
        main.add(jiraPassword,cs);

        testConnection = new JButton("Test Connection");
        cs.gridx=0;
        cs.gridy=5;
        cs.weightx=1;
        cs.anchor = GridBagConstraints.CENTER;
        cs.insets = new Insets(10,0,0,0);
        cs.gridwidth=2;

        main.add(testConnection,cs);

        connTest = new JLabel("");
        cs.gridx=0;
        cs.gridy=6;
        cs.insets = new Insets(0,0,0,0);
        main.add(connTest,cs);

        info = new JLabel("");
        cs.gridx=0;
        cs.gridy=7;
        main.add(info,cs);

        summary = new JLabel("");
        cs.gridx=0;
        cs.gridy=8;
        main.add(summary,cs);

        reporter = new JLabel("");
        cs.gridx=0;
        cs.gridy=9;
        main.add(reporter,cs);

        assignee = new JLabel("");
        cs.gridx=0;
        cs.gridy=10;
        main.add(assignee,cs);

        jiraSite.setText(JIRA_URL);
        jiraUser.setText(JIRA_USERNAME);
        jiraParentTicket.setText(JIRA_TICKET);
        jiraAssignee.setText(JIRA_DEFAULT_ASSIGNEE);

        testConnection.addActionListener(new ActionListener() {@Override
                public void actionPerformed(ActionEvent actionEvent) {
            testConnection.setEnabled(false);
            testConnect(jiraUser.getText(), jiraPassword.getPassword(), jiraSite.getText(), jiraParentTicket.getText());
            info.setText("Please wait, trying to connect...");
            //TODO some logic for missing fields
        }
        });

        return main;
    }

    public void testConnect(final String u,final char[] p,final String j, final String i){
        //Start a new background thread that doesn't freeze the main UI :D
        Executors.newSingleThreadExecutor().execute(new Runnable() {
            @Override
            public void run() {
                try {
                    creds = new BasicCredentials(u, new String(p));
                    jira = new JiraClient(j, creds);
                    issue = jira.getIssue(i);

                    //Simple logging to the extension output
                    out.println("This is the issue:" + issue);
                    out.println("issue summary: " + issue.getSummary());
                    out.println("Issue Assignee: " + issue.getAssignee());
                    out.println("Issue Reporter: " + issue.getReporter());
                    out.println("issue discription: " + issue.getDescription());
                    out.println("");
                    testConnection.setEnabled(true);
                    info.setText("Issue: " + issue.getKey());
                    summary.setText("Summary: " + issue.getSummary());
                    reporter.setText("Reporter: " + issue.getReporter());
                    assignee.setText("Assignee: " + issue.getAssignee());
                    connTest.setText("Success :D...");

                } catch (JiraException e) {
                    err.println(e);
                    info.setText("");
                    reporter.setText("");
                    assignee.setText("");
                    connTest.setText("Failed fix something...");
                    testConnection.setEnabled(true);
                }
                creds = null;
                jira = null;
                issue = null;
            }

        });
    }

    public void postIssue(final String vuln, final String desc, final String p){

        Executors.newSingleThreadExecutor().execute(new Runnable() {
            @Override
            public void run() {
                try {
                    out.println("this is the Vuln: " + vuln );
                    out.println("This is desc:\n" + desc);
                    out.println("This is priority: " + p);
                    if (getAssignee().equals("Unassigned")){
                        out.println("No assignee");
                    }
                } catch (Exception e) {
                    out.println("This is err: " + e);

                }
                try {
                    creds = new BasicCredentials(getUsername(), new String(getPassword()));
                    jira = new JiraClient(getJira(), creds);
                    issue = jira.getIssue(getTicket());
                    if (getAssignee().isEmpty() || getAssignee().equals("Unassigned")) {

                        issue.createSubtask().
                                field(Field.SUMMARY, vuln).
                                field(Field.DESCRIPTION, desc).
                                field(Field.ISSUE_TYPE, "Sub-task").
                                field(Field.PRIORITY, Field.valueById(p)).
                                field(Field.ASSIGNEE, "")
                                .execute();
                        out.println("Successfully posted to JIRA");
                    } else {
                        issue.createSubtask().
                                field(Field.SUMMARY, vuln).
                                field(Field.DESCRIPTION, desc).
                                field(Field.ISSUE_TYPE, "Sub-task").
                                field(Field.PRIORITY, Field.valueById(p)).
                                field(Field.ASSIGNEE, getAssignee())
                                .execute();
                        out.println("Successfully posted to JIRA");
                    }
                } catch (JiraException e) {
                    out.println("Something went wrong...");
                    out.println(e);
                }
                creds = null;
                jira = null;
                issue = null;
            }
        });
    }

    //Getters...
    private String getUsername(){
        return jiraUser.getText();
    }

    private char[] getPassword(){
        return jiraPassword.getPassword();
    }

    private String getJira(){
        return jiraSite.getText();
    }

    private String getTicket(){
        return jiraParentTicket.getText();
    }

    private String getAssignee(){
        return jiraAssignee.getText();
    }

}

